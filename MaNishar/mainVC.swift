//
//  ViewController.swift
//  MaNishar
//
//  Created by Ohad on 26.12.2018.
//  Copyright © 2018 Ohad. All rights reserved.
//
import GoogleAPIClientForREST
import GoogleSignIn
import UIKit
import WatchConnectivity

class mainVC: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {

    

    @IBOutlet weak var view_main: UIView!
    @IBOutlet weak var button_done: UIButton!
    @IBOutlet weak var textView_comment: UITextField!
    @IBOutlet weak var textView_amount: UITextField!
    @IBOutlet weak var textView_category: UITextField!
    
    // Ohad`s ID
    let spreadsheetId = "1DKNC8Rsd7Wqav59wblqbCK6WU9IDdnHQm7qOjhgJxy4"
    // Coral`s and Tal`s ID
    //let spreadsheetId = "1UREIMSoOlEqkaVIkux16pnK7mVxRsR0Zf_sSjtaIhi0"

    let RANGE_AVILABLE_RAW = "data!b45:b45"
    let RANGE_CATEGORIES = "data!l2:l23"
    var categories: [String] = [String]()
    // If modifying these scopes, delete your previously saved credentials by
    // resetting the iOS simulator or uninstall the app.
    private let scopes = [kGTLRAuthScopeSheetsSpreadsheets]
    
    private let service = GTLRSheetsService()
    let signInButton = GIDSignInButton()
    let output = UITextView()
    static let SETTINGS_KEY_SHEETS_ID = "sheet_id"
    var avilableRaw_str: String = ""
    var avilableRaw_int: Int = 0
    let picker_category = UIPickerView()
//    var session: WCSession?//2

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureWatchKitSesstion()

        /* Hide the button till we will validate if the user signed in */
        view_main.isHidden = true
        picker_category.delegate = self
        picker_category.dataSource = self
        textView_category.inputView = picker_category
        textView_amount.keyboardType = .decimalPad
        textView_comment.keyboardType = .default
        registerSettingsBundle()
        
        //let settings = UserDefaults.standardUserDefaults()
        //let value = settings.stringForKey("key")! as String!
        
        // TODO(developer) Configure the sign-in button look/feel
        // ...
        // Configure Google Sign-in.
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().scopes = scopes
        GIDSignIn.sharedInstance().signInSilently()
        
        // Add the sign-in button.
        view.addSubview(signInButton)
        
        // Add a UITextView to display output.
        //output.frame = view.bounds
        //output.isEditable = false
        //output.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        //output.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        //output.isHidden = true
        //view.addSubview(output);
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func configureWatchKitSesstion() {
        if WCSession.isSupported() {
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
    }

    func registerSettingsBundle(){
        let appDefaults = [String:AnyObject]()
        UserDefaults.standard.register(defaults: appDefaults)
    }
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            showAlert(title: "Authentication Error", message: error.localizedDescription)
            self.service.authorizer = nil
        } else {
            //DispatchQueue.main.async {
            self.view_main.isHidden = false
            self.signInButton.isHidden = true
            self.output.isHidden = false
            //}
            self.service.authorizer = user.authentication.fetcherAuthorizer()
            
            /* Get the avilable raw to insert data */
            let selector_avilableRaw = #selector(self.getAvilabelRaw(ticket:finishedWithObject:error:))
            getValues(range: RANGE_AVILABLE_RAW, f: selector_avilableRaw)
            
            let selector_getCategory = #selector(self.getCategory(ticket:finishedWithObject:error:))
            getValues(range: RANGE_CATEGORIES, f: selector_getCategory)
            
        }
    }

    
    // Display (in the UITextView) the names and majors of students in a sample
    // spreadsheet:
    // https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
    func getValues(range: String, f: Selector) {
        let query = GTLRSheetsQuery_SpreadsheetsValuesGet
            .query(withSpreadsheetId: spreadsheetId, range:range)
        service.executeQuery(query,
                             delegate: self,
                             didFinish: f)
    }
    
    
    func setValues(range: String, valueRange: GTLRSheets_ValueRange,  f: Selector) {
        let query = GTLRSheetsQuery_SpreadsheetsValuesUpdate.query(withObject: valueRange, spreadsheetId: spreadsheetId, range: range)
        service.executeQuery(query,
                             delegate: self,
                             didFinish: f)
    }
    
    // Process the response and display output
    @objc func getAvilabelRaw(ticket: GTLRServiceTicket,
                                 finishedWithObject result : GTLRSheets_ValueRange,
                                 error : NSError?) {
        
        if let error = error {
            showAlert(title: "Error", message: error.localizedDescription)
            return
        }
        
        var raws = result.values!
        self.avilableRaw_str = raws[0][0] as! String
        self.avilableRaw_int = Int(self.avilableRaw_str) ?? 0
        
    }
    
    // Process the response and display output
    @objc func getCategory(ticket: GTLRServiceTicket,
                              finishedWithObject result : GTLRSheets_ValueRange,
                              error : NSError?) {
        
        if let error = error {
            showAlert(title: "Error", message: error.localizedDescription)
            return
        }
        
        for row in result.values! {
            categories.append(row[0] as! String)
        }
        
        
    }
    
    // Process the response and display output
    @objc func setExpanceDone(ticket: GTLRServiceTicket,
                           finishedWithObject result : GTLRSheets_ValueRange,
                           error : NSError?) {
        
        if let error = error {
            showAlert(title: "Error", message: error.localizedDescription)
            return
        }
        
    }
    
    // Helper for showing an alert
    func showAlert(title : String, message: String) {
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: UIAlertController.Style.alert
        )
        let ok = UIAlertAction(
            title: "OK",
            style: UIAlertAction.Style.default,
            handler: nil
        )
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
    }
    
    //MARK: Picker View Delegates
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if WCSession.default.activationState == .activated {
            do {
                try WCSession.default.updateApplicationContext(["categories": categories])
            } catch {
                print(error)
              }
         }
        return categories.count
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return categories[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        textView_category.text = categories[row]
        textView_amount.becomeFirstResponder()
    }
    
    @IBAction func buttonDoneClicked(_ sender: Any) {
        if ((textView_category.text?.isEmpty)! || (textView_amount.text?.isEmpty)!) {
            // display alert
            return
        }
        
        let range = "נתונים!D\(avilableRaw_int):F\(avilableRaw_int)"
        //setValues(range: range, valueRange: valueRange, f: selector_setExpanceDone)
        let values = [[textView_category.text, textView_amount.text, textView_comment.text]]
        
        self.SetExpance(range: range, values: values as! [[String]])
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case self.textView_amount:
            self.textView_comment.becomeFirstResponder()
            break
        case self.textView_comment:
            textField.endEditing(true)
            break
        default:
            break
        }
        return true
    }
    
    
    func updateExpance(range: String, values: [[String]] ) {
        let valueRange = GTLRSheets_ValueRange() // GTLRSheets_ValueRange holds the updated values and other params
        valueRange.majorDimension = "ROWS" // Indicates horizontal row insert
        valueRange.range = range
        valueRange.values = values
        
        let query = GTLRSheetsQuery_SpreadsheetsValuesAppend.query(withObject: valueRange, spreadsheetId: spreadsheetId, range: range) // Use an append query to append at the first blank row
        query.valueInputOption = "USER_ENTERED"
        service.executeQuery(query) { ticket, object, error in
            if error == nil {
                self.clearTextFields()
                self.avilableRaw_int = self.avilableRaw_int + 1
                self.showAlert(title: "Succedded", message: "הנתונים הוזנו בהצלחה")
            } else {
                self.showAlert(title: "Error", message: error?.localizedDescription ?? "We have error")
                // we have error
            }
            print("object")
        } // `GTLRServiceCompletionHandler` closure containing the service ticket, `GTLRSheets_AppendValuesResponse`, and any error
    }

    func SetExpance(range: String, values: [[String]] ) {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yy"
        let date_str = formatter.string(from: date)
        
        let date_range = "'נתונים'!A\(avilableRaw_int):A\(avilableRaw_int)"
        
        let valueRange = GTLRSheets_ValueRange() // GTLRSheets_ValueRange holds the updated values and other params
        valueRange.majorDimension = "ROWS" // Indicates horizontal row insert
        valueRange.range = date_range
        valueRange.values = [[date_str]]
        
        let query = GTLRSheetsQuery_SpreadsheetsValuesAppend.query(withObject: valueRange, spreadsheetId: spreadsheetId, range: date_range) // Use an append query to append at the first blank row
        query.valueInputOption = "USER_ENTERED"
        service.executeQuery(query) { ticket, object, error in
            if error == nil {
                self.updateExpance(range: range, values: values)
            } else {
                self.showAlert(title: "Error", message: error?.localizedDescription ?? "We have error")
                // we have error
            }
            print("object")
        } // `GTLRServiceCompletionHandler` closure containing the service ticket, `GTLRSheets_AppendValuesResponse`, and any error
    }

    func clearTextFields() {
        textView_comment.text = ""
        textView_amount.text = ""
        textView_category.text = ""
    }
}

// WCSession delegate functions
extension mainVC: WCSessionDelegate {
  
    func sessionDidBecomeInactive(_ session: WCSession) {
    }

    func sessionDidDeactivate(_ session: WCSession) {
        WCSession.default.activate()
    }

    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        do {
            try WCSession.default.updateApplicationContext(["categories": categories])
        } catch {
            print(error)
          }
    }

    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("received message: \(message)")
        DispatchQueue.main.async { //6
          if let value = message["watch"] as? String {
        //        self.label.text = value
          }
    }
    }
}


