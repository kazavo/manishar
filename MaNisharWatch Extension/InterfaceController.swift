//
//  InterfaceController.swift
//  MaNisharWatch Extension
//
//  Created by Ohad Katzav on 15/02/2021.
//  Copyright © 2021 Ohad. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class InterfaceController: WKInterfaceController {

    @IBOutlet weak var button_submit: WKInterfaceButton!
    @IBOutlet weak var textView_comment: WKInterfaceTextField!
    @IBOutlet weak var textView_amount: WKInterfaceTextField!
    @IBOutlet weak var picker_category: WKInterfacePicker!
        
    override func awake(withContext context: Any?) {
        // Configure interface objects here.

        if WCSession.isSupported() {
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
    }
    
    @IBAction func submit() {
        print("submit")
    }
    
}


extension InterfaceController: WCSessionDelegate {
  
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        if let error = error {
            print("Activation failed with error: \(error.localizedDescription)")
            return
        }
        print("Watch activated with state: \(activationState.rawValue)")
    }

    func session(_ session: WCSession, didReceiveApplicationContext applicationContext: [String : Any]) {
        if let categories = applicationContext["categories"] as? [String]{
            print("\(categories)")
            var pickerItems: [WKPickerItem] = [WKPickerItem]()
            for cat in categories {
                let pickerItem = WKPickerItem()
                pickerItem.title = cat
                pickerItems.append(pickerItem)
            }
            self.picker_category.setItems(pickerItems)
        }
    }
    
//    func sessionDidBecomeInactive(_ session: WCSession) {
//    }
    
    func sessionDidDeactivate(session: WCSession) {
        // Begin the activation process for the new Apple Watch.
        WCSession.default.activate()
    }


    
}
